package assignment_project;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;

public class MarkSweepGC extends GC{
	private List<Block> freeList; // store the freed blocks
	private Block initialBlk; // This is the initial block

	public MarkSweepGC(int heapSize, double p, double t) {
		super(heapSize, p, t);		
		freeList = new LinkedList<Block>();
		initialBlk = new Block(0, heapSize);
		freeList.add(initialBlk); // the heap initial state block is added to free list								
	}

	/*
	 * This method is to allocate heap. The free list is traversed to find
	 * available space to fit in the block; if find one, store it to "idArr"
	 * array, and update the free list
	 */
	public int allocate(Object obj) {
		int objId = obj.getObjId();
		int result = -1;
		if (objId != 0) {			
			int objSize = obj.getObjSize();			
			ListIterator<Block> freeListIterater = freeList.listIterator();
			while (freeListIterater.hasNext()) {
				Block freeBlk = freeListIterater.next();
				int freeBlkSize = freeBlk.getBlkSize();
				
				if (freeBlkSize >= objSize) {
					// if find an available freed block,
					int freeBlkStartPt = freeBlk.getStartPt();
					
					int objAddr = freeBlkStartPt;
					obj.setAddr(objAddr);
					objMap.put(objId, obj);
					// update free block in free list
					int newFreeBlkStartPt = objAddr + objSize;
					int newFreeBlkSize = freeBlkSize - objSize;
					if (newFreeBlkSize == 0) {	
						// if the available block has used up(best fit), then remove this block from free list
						freeList.remove(freeBlk);
					}else{
						// update the free list with updated free block
						freeBlk.setStartPt(newFreeBlkStartPt);
						freeBlk.setBlkSize(newFreeBlkSize);
					}								
					heapUsage += objSize;
					liveObjCount++;
					return objAddr;
				}
			} // end while loop		
			return result;
		} else {
			return -2;
		}

	}

	public void runGC() {
		startTime = System.nanoTime();
		gcCounts++;
		for (Object obj : rootset) {
			mark(obj);				
		}
		sweep();
		endTime   = System.nanoTime();
		long totalTime = endTime - startTime;
		gcRunningTime += totalTime;
	}

	private void mark(Object obj) {
		if (!obj.isMarked()) {
			obj.setMarked(true);
			Object[] chRefs = obj.getRefSlots();
			for (Object chObj : chRefs) {
				if(chObj != null)
					mark(chObj);
			}
		}
	}

	private void sweep() {
		// use iterator to avoid concurrentModificationException exception
		Iterator<Entry<Integer, Object>> it = objMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, Object> pair = (Map.Entry<Integer, Object>) it.next();
			Object obj = pair.getValue();
			if (obj.isMarked()) {
				obj.setMarked(false);
			} else {
				it.remove();	
				free(obj);
			}
		}
	}

	public void increaseHeapSize() {
		// update freeList
		int increased = (int) (heapSize * p);
		if (freeList.size() > 0) {
			Block lastBlk = ((LinkedList<Block>) freeList).getLast();			
			int lastFreedBlkNewSize = lastBlk.getBlkSize() + increased;
			lastBlk.setBlkSize(lastFreedBlkNewSize);
		}else{
			Block newBlk = new Block(0, increased);
			freeList.add(newBlk);
		}
		heapSize += increased;
	}


	/*
	 * This method is to free blocks from heap by address passed. Traverse the
	 * "freeList" linked list to find the proper position where insert a new
	 * free block by calling searchFreeList() method searchFreeList() method
	 * return a index of proper position in "freeList" linked list, each return
	 * value has different situation to be considered: 1. -1 -> fail to find; 2.
	 * "-2" -> insert the block in very first position. Two situations need to
	 * be considered 2.1. if its back is free block, then merge it and update
	 * the free list 2.2. if not, insert it to very first position 3. "-3" ->
	 * insert the block very last position. 4. index of proper position 4.1 if
	 * its front is also a freed block and back is allocated block. merge it and
	 * update the free list 4.2 if its back is also a freed block and front is
	 * allocated block. merge it and update the free list 4.3 if its front and
	 * back are both freed blocks, merge them to a large free space and update
	 * the free list 4.4 if its front and back are both allocated blocks, then
	 * insert this free block to free list
	 */
	protected void free(Object obj) {
		//System.out.println("======================");
		/*System.out.println("Free Object: " + obj.toString() + "\nBefore Freeing: ");
		printFreeList();*/
		int addrs = obj.getAddr();
		int size = obj.getObjSize();
		Block curBlk = new Block(addrs, size);
		if (freeList.size() > 0) {
			Block firstBlk = ((LinkedList<Block>) freeList).getFirst();
			Block lastBlk = ((LinkedList<Block>) freeList).getLast();
			int preBlkIndex = searchFreeList(curBlk);
			if (preBlkIndex == -2) {
				// case 2
				if (isMerged(curBlk, firstBlk)) {
					Block mergedBlk = mergeFreeBlk(curBlk, firstBlk);
					freeList.set(0, mergedBlk);
				} else {
					freeList.add(0, curBlk);
				}
			} else if (preBlkIndex == -3) {
				// case 3
				if (isMerged(lastBlk, curBlk)) {
					Block mergedBlk = mergeFreeBlk(lastBlk, curBlk);
					freeList.set(freeList.size() - 1, mergedBlk);
				} else {
					freeList.add(curBlk);
				}

			} else if (preBlkIndex >= 0) {
				// case 4
				Block preBlk = freeList.get(preBlkIndex);
				// if to be freed block is neither very first nor very last
				int nextBlkIndex = preBlkIndex + 1;
				Block nextBlk = freeList.get(nextBlkIndex);
				if (isMerged(preBlk, curBlk)) {
					// if previous block and current block need to be merged, then do merging
					Block mergedBlk = mergeFreeBlk(preBlk, curBlk);
					if (isMerged(mergedBlk, nextBlk)) {
						// if merged block and next block need to be merged,then merge them; and update the free list						
						Block newMergedBlk = mergeFreeBlk(mergedBlk, nextBlk);
						freeList.set(preBlkIndex, newMergedBlk);
						freeList.remove(nextBlk);
					} else {
						// if not, update the free list
						freeList.set(preBlkIndex, mergedBlk);
					}
				} else {
					// if not, then check if current block and next block need to be merged or not
					if (isMerged(curBlk, nextBlk)) {
						// if current block and next block need to be merged, then merge them and update free list
						Block mergedBlk = mergeFreeBlk(curBlk, nextBlk);
						freeList.set(nextBlkIndex, mergedBlk);
					} else {
						// if not, insert current block between previous block and next block
						freeList.add(nextBlkIndex, curBlk);
					}
				}
			} else {
				// case 1
				System.out.println("Error occured!");
				return;
			}
		} else {
			freeList.add(curBlk);
		}
		//System.out.println("Free Object: " + obj.getObjId());
		/*System.out.println("After Freeing:");
		printFreeList();	*/	
		heapUsage -= obj.getObjSize();
		objMap.remove(obj.getObjId()); // update object map
	}

	// this method is to merge neighbored blocks to a large block
	private Block mergeFreeBlk(Block b1, Block b2) {
		Block newBlk = null;
		int startPt = b1.getStartPt();
		int b1Size = b1.getBlkSize();
		int b2Size = b2.getBlkSize();
		int newBlkSize = b1Size + b2Size;
		newBlk = new Block(startPt, newBlkSize);
		return newBlk;
	} // end mergeFreeBlk()

	// this method is to determine if two blocks are both freed
	private boolean isMerged(Block b1, Block b2) {
		int b1StartPt = b1.getStartPt();
		int b1Size = b1.getBlkSize();
		int b1EndPt = b1StartPt + b1Size;
		int b2StartPt = b2.getStartPt();
		// if b1 and b2 both point to the same address space, then b1 and b2
		// need to be merged
		if (b1EndPt == b2StartPt)
			return true;
		return false;
	} // end isMerged()

	// This method is to search a proper position where insert the free block
	// return -1, if not found one
	// return -2, if free block needs to be inserted in very first position
	// return -3, if free block needs to be inserted in very last position
	// return index of proper position, if free block needs to be inserted in
	// neither very first nor very last position
	private int searchFreeList(Block curBlk) {
		Block preBlk = null;
		Block nextBlk = null;
		Iterator<Block> iterator = freeList.iterator();
		int curBlkStartPt = curBlk.getStartPt();
		int curBlkSize = curBlk.getBlkSize();
		int curBlkEndPt = curBlkStartPt + curBlkSize;
		int freeListSize = freeList.size();
		int i = 0;
		while(iterator.hasNext()){
			preBlk = iterator.next();
			int preBlkStartPt = preBlk.getStartPt();
			int preBlkSize = preBlk.getBlkSize();
			int preBlkEndPt = preBlkStartPt + preBlkSize;
			if (preBlkStartPt >= curBlkEndPt) {
				return -2;
			} else if (preBlkEndPt <= curBlkStartPt) {
				if (i == freeListSize - 1) {
					// if there is only one block in free list
					return -3;
				} else {
					nextBlk = freeList.get(i + 1);
					int nextBlkStartPt = nextBlk.getStartPt();
					if (curBlkEndPt <= nextBlkStartPt) {
						return i;
					}
				}
			}	
			i++;
		}
		return -1;
	} // end searchFreeList()

	public void stats() {
		System.out.println("Free List Status: ");
		int freeSize = heapSize - heapUsage;
		int freeListLen = freeList.size();
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		double avgSize = (double) freeSize / (double) freeListLen;		
		System.out.println("Number in the free list: " + freeListLen);
		System.out.println("Average size in the free list: " + df.format(avgSize));
		super.stats();
		
	}// end stats()

	
	public List<Block> getFreeList() {
		return freeList;
	}

	public void setFreeList(List<Block> freeList) {
		this.freeList = freeList;
	}

	protected void printFreeList() {
		for (Block blk : freeList) {
			System.out.println(blk.toString());
		}
	}
	
}
