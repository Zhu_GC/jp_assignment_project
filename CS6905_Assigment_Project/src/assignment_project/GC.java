package assignment_project;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class GC implements GCInterface{
	
	protected List<Object> rootset;
	protected Map<Integer, Object> objMap;
	protected int heapSize;
	protected double p;
	protected double t;
	protected int heapUsage;
	protected int liveObjCount;
	protected int gcCounts;	
	protected long startTime, endTime, gcRunningTime;

	public GC(int sizeOfHeap, double p, double t) {
		this.heapSize = sizeOfHeap;
		this.p = p;
		this.t = t;
		rootset = new LinkedList<Object>();
		objMap = new HashMap<Integer, Object>();
		heapUsage = 0;
		liveObjCount = 0;
		gcCounts = 0;
		gcRunningTime = 0l;
	}

	@Override
	public int addRefToRootSet(int objId) {
		Object refObj = objMap.get(objId);
		if (refObj != null) {
			rootset.add(refObj);
			return 1;
		}
		return -1;
	}
	
	@Override
	public int deleteRefFromRootSet(int objId) {
		Object refObj = objMap.get(objId);
		if (refObj != null) {
			rootset.remove(refObj);
			return 1;
		}
		return -1;
	}
	
	@Override
	public int writeObjRef(int pId, int chId, int slotNum) {
		int flag = -1;
		Object pObj = objMap.get(pId);
		Object chObj = objMap.get(chId);
		if (pObj != null && chObj != null) {
			pObj.getRefSlots()[slotNum] = chObj;
			flag = 1;
		}
		return flag;
	}
	
	@Override
	public int allocate(Object obj) {
		return -1;
	}
	
	@Override
	public void runGC() {
		
	}
	
	@Override
	public void increaseHeapSize() {
		//double curT = (double) heapUsage / (double) heapSize;
		
		System.out.println("Start Increasing...");
	}
	
	@Override
	public boolean isExpanded() {	
		double curT = (double) heapUsage / (double) heapSize;	
		/*boolean isE = curT >= t;
		System.out.println(heapUsage + " / " + heapSize + " = " + curT + "\t" + isE);*/
		return (curT >= t);
	}
	
	@Override
	public void stats() {
		double heapUsageP = (double) heapUsage / (double) heapSize;
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		long gcTimeMs = gcRunningTime / 1000000;
		System.out.println("Total Heap Size: " + heapSize + "\tHeap Usage: " + heapUsage);
		System.out.println("Heap Usage %: " + df.format(heapUsageP * 100.0));
		System.out.println("Number of live objects are: " + objMap.size());
		System.out.println("GC Times: " + gcCounts + " in " + gcTimeMs + "ms");
		
	}
	
	@Override
	public void collectData() {
		System.out.print(heapSize + "," + heapUsage + ",");
	}
	
	protected void printObjMap(Map<String, Object> mp) { 
		for(Entry<Integer, Object>entry :objMap.entrySet()){
			entry.getKey(); 
			System.out.println(entry.getValue().toString()); 
		} 
	}
	
	public int getGcCounts() {
		return gcCounts;
	}

	public void setGcCounts(int gcCounts) {
		this.gcCounts = gcCounts;
	}

	public boolean isExtend() {
		// TODO Auto-generated method stub
		return false;
	}
	
	
}
