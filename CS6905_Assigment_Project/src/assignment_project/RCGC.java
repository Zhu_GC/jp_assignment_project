package assignment_project;

public class RCGC extends MarkSweepGC{

	public RCGC(int sizeOfHeap, double p, double t) {
		super(sizeOfHeap, p, t);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int writeObjRef(int pId, int chId, int refSlotNum) {
		int flag = -1;
		Object pObj = objMap.get(pId);
		Object chObj = objMap.get(chId);
		if (pObj != null && chObj != null) {
			Object oldRef = pObj.getRefSlots()[refSlotNum];
			flag = 1;
			addReference(chObj);
			if(oldRef != null)				
				deleteReference(oldRef);
			pObj.getRefSlots()[refSlotNum] = chObj;
		}
		return flag;
	}
	
	@Override
	public int addRefToRootSet(int objId) {
		Object refObj = objMap.get(objId);
		if (refObj != null) {
			addReference(refObj);
			rootset.add(refObj);
			return 1;
		}
		return -1;
	}
	
	@Override
	public int deleteRefFromRootSet(int objId) {
		Object refObj = objMap.get(objId);
		if (refObj != null) {
			deleteReference(refObj);
			rootset.remove(refObj);
			return 1;
		}
		return -1;
	}
	

	private int addReference(Object ref){
		int count = -1;	
		if(ref != null){
			count = ref.getRefCount();
			count++;
			ref.setRefCount(count);
		}		
		return count;
	}
	
	private int deleteReference(Object ref){
		int count = -1;	
		if(ref != null){
			count = ref.getRefCount();
			count--;
			
			if(count == 0){
				//System.out.println("Object " + ref.getObjId() + " counts become ZERO!!!");
				Object[] chRefs = ref.getRefSlots();
				for(Object chObj : chRefs){
					if(chObj != null){
						deleteReference(chObj);
						//System.out.println("\t\tChild Object: " + chObj.toString());
					}						
				}
				free(ref);
			}else{
				ref.setRefCount(count);
			}			
		}		
		return count;
	}
}
