package assignment_project;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class CopyGC extends GC{
	
	private List<Object> workList;
	private int scanPt, freePt;
	private int toSpacePt, fromSpacePt, topPt, extent;
	private int heapStart, heapEnd;
	
	public CopyGC(int sizeOfHeap, double p, double t){
		super(sizeOfHeap, p, t);
		heapStart = 0;
		heapEnd = heapSize;
		rootset = new LinkedList<Object>();
		workList = new LinkedList<Object>();
		objMap = new HashMap<Integer, Object>();
		createSemiSpace();
	}
	
	public void runGC(){
		//System.out.println("GC starts work...");
		heapUsage = 0;
		//System.out.println("Heap Size: " + heapSize + "\tToSpace Pointer: " + toSpacePt);
		startTime = System.nanoTime();
		gcCounts++;
		collect();
		//heapUsage = freePt - toSpacePt;
		endTime   = System.nanoTime();
		long totalTime = endTime - startTime;
		gcRunningTime += totalTime;
		//System.out.println("GC ends work...");
	}
	
	private void createSemiSpace(){
		toSpacePt = heapStart;
		extent = (heapEnd - heapStart) / 2;
		topPt = heapStart + extent;	
		fromSpacePt = topPt;
		freePt = toSpacePt;	
	}
	
	@Override
	public int allocate(Object obj){
		int objId = obj.getObjId();
		int objSize = obj.getObjSize();
		if (objId != 0) {
			int result = freePt;
			int newFreePt = result + objSize;
			//System.out.println("newFreePt: " + newFreePt + "\ttopPt: " + topPt + "\tHeap Usage: " + tmp + " extent: " + extent);
			if(newFreePt > topPt){
				return -1;
			}			
			obj.setAddr(result);
			freePt = newFreePt;
			heapUsage += objSize;
			//heapUsage = freePt - toSpacePt;
			objMap.put(objId, obj);
			//System.out.println("Allocated ID: " + objId + "\tAllocated Size: " + objSize + "\tHeap Usage: " + heapUsage + "\tToSpace: " + toSpacePt);
			//System.out.println("Allocate Object: " + obj.toString() + "\t\tHeap Usage: " + heapUsage);
			return result;
		} else{
			// NULL object cannot be allocated, return -2
			return -2;
		}		
	}
	
	private void collect(){
		//super.heapUsage = 0;
		int workListIndex = 0;
		flip();
		initWorkList();
		for(Object obj : rootset){
			process(obj);
		}
		while(!isEmpty()){
			Object scannedObj = remove(workListIndex);
			if(scannedObj != null){
				scan(scannedObj);
				workListIndex++;
			}else{
				break;
			}
		}
		workList.clear();
	}
	
	private void flip(){
		int tmpFromSpacePt = fromSpacePt;
		fromSpacePt = toSpacePt;
		toSpacePt = tmpFromSpacePt;
		topPt = toSpacePt + extent;
		freePt = toSpacePt;
	}
	
	private void scan(Object obj){
		Object[] chRefs = obj.getRefSlots();
		for(Object ch : chRefs){
			if(ch != null)
				process(ch);
				
		}
	}
	
	private void process(Object obj){
		Object fromObj = obj;
		if(fromObj != null){
			obj = forward(fromObj);
		}
	}
	
	private Object forward(Object fromObj){
		int toObjForwardingAddr = fromObj.getForwardingAddr();
		Object toObj = null;
		if(toObjForwardingAddr == -1){			
			toObj = copy(fromObj);
		}
		return toObj;
	}
	
	private Object copy(Object fromObj){
		int toObjStartPt = freePt;
		int fromObjSize = fromObj.getObjSize();
		int fromObjRefSlot = fromObj.getNumOfRefSlots();
		int indexOfFromObj = rootset.indexOf(fromObj);
		if(indexOfFromObj != -1){
			Object toObj = new Object(toObjStartPt, fromObjSize, fromObjRefSlot);
			move(fromObj, toObj);
			fromObj.setForwardingAddr(toObjStartPt);
			workList.add(toObj);
			freePt += fromObjSize;
			heapUsage += fromObjSize;
			//heapUsage = freePt-toSpacePt;
		   // System.out.println("Copy ID: " + fromObj.getObjId() + "\tCopy Size: " + fromObjSize + "\tHeap Usage: " + heapUsage);
			//System.out.println("Copy Object: " + toObj.toString() + "\t\tHeap Usage: " + heapUsage);
			rootset.set(indexOfFromObj, toObj);
			return toObj;
		}
		return null;
	}
	
	private void move(Object fromObj, Object toObj){
		toObj.setObjId(fromObj.getObjId());
		Object[] chRefs = fromObj.getRefSlots();
		toObj.setRefSlots(chRefs);	
	}
	
	private void initWorkList(){
		scanPt = freePt;
	}
	
	private boolean isEmpty(){
		return scanPt == freePt;
	}
	
	private Object remove(int index){
		if(workList.size() > 0){
			// return the scanned object (turn grey objects to black)
			// update the scan pointer
			Object scannedObj = workList.get(index);
			scanPt = scanPt + scannedObj.getObjSize();
			return scannedObj;
		}else{
			return null;
		}
		
	}
	
	/*public boolean isExtend(){
		double curT = (double) heapUsage / (double) extent;	
		boolean isE = curT >= t;
		System.out.println(heapUsage + " / " + heapSize + " = " + curT + "\t" + isE);
		return (curT >= t);
	}*/
	
	public void increaseHeapSize() {
		this.heapEnd = (int) (this.heapEnd * (1 + this.p));
		heapSize = heapEnd;
		extent = (heapEnd - heapStart) / 2;
		toSpacePt = (int) (toSpacePt * (1 + this.p));
		fromSpacePt = (int) (fromSpacePt * (1 + this.p));
		topPt = (int) (topPt * (1 + this.p));
		freePt = (int) (freePt * (1 + this.p));
		scanPt = (int) (scanPt * (1 + this.p));
		//System.out.println("Increased Heap Size " + heapSize + "\tFree Ptr: " + freePt);
	}
	
	public void stats() {
		super.stats();
	}// end stats()
	
}
