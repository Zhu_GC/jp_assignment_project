package assignment_project;

public class Constances {
	public static String ALLOCATION = "a";
	public static String PLUS = "+";
	public static String STORE = "s";
	public static String WRITE = "w";
	public static String CLASS = "c";
	public static String READ = "r";
	public static String DELETE = "-";
	public static String DELIMETER = " ";
	public static String COPY = "cp";
	public static String MS = "ms";
	public static String ORC = "optRC";
	public static String RC = "rc";
}
