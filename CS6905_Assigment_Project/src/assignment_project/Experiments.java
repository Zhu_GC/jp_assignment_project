package assignment_project;

public class Experiments {

	private DataProcess dataPro;
	
	public Experiments(){
		dataPro = new DataProcess();
	}
	
	public void performExp(int heapSize, String traceFileName, String gcChoice) {
		double[] pArr = { 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1 };
		double[] tArr = { 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1 };
		System.out.println("Heap Size, P Value, t Value");
		for (double p : pArr) {
			for (double t : tArr) {
				dataPro.scanFile(heapSize, p, t, traceFileName, gcChoice);
			}
		}
	}

}
