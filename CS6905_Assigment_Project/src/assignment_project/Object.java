package assignment_project;

public class Object {
	private int addr;
	private int objId;
	private int objSize;
	private boolean isMarked;
	private boolean isNew;
	private int forwardingAddr;
	private int refCount;
	private Object[] refSlots;
	private int numOfRefSlots;
	
	public Object(int objId, int objSize, int numOfRefSlots){
		this.objId = objId;
		this.objSize = objSize;
		this.forwardingAddr = -1;
		this.numOfRefSlots = numOfRefSlots;
		refSlots = new Object[numOfRefSlots];
		refCount = 0;
		isNew = true;
	}
	
	public int getObjId() {
		return objId;
	}

	public void setObjId(int objId) {
		this.objId = objId;
	}

	public int getObjSize() {
		return objSize;
	}

	public void setObjSize(int objSize) {
		this.objSize = objSize;
	}

	public int getAddr() {
		return addr;
	}

	public void setAddr(int addr) {
		this.addr = addr;
	}
	
	
	public boolean isMarked() {
		return isMarked;
	}

	public void setMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}
	
	public int getForwardingAddr() {
		return forwardingAddr;
	}


	public void setForwardingAddr(int forwardingAdd) {
		this.forwardingAddr = forwardingAdd;
	}
	
	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public int getRefCount() {
		return refCount;
	}


	public void setRefCount(int refCount) {
		this.refCount = refCount;
	}

	
	public Object[] getRefSlots() {
		return refSlots;
	}

	public void setRefSlots(Object[] refSlot) {
		this.refSlots = refSlot;
	}
	
	

	public int getNumOfRefSlots() {
		return numOfRefSlots;
	}

	public void setNumOfRefSlots(int numOfRefSlots) {
		this.numOfRefSlots = numOfRefSlots;
	}

	public String toString(){
		int endPt = addr + objSize;
		return "Object ID: " + objId + " Address: " + addr  +
				" Size: " + objSize + " End Pointer: " + endPt + " Counts " + refCount;
	}

}
