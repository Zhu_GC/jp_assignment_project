package assignment_project;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class DataProcess {
	

	public void scanFile(int heapSize, double p, double t, String traceFileName, String gcChoice) {
		String fileName = traceFileName;
		File file = new File(fileName);
		Scanner sc = null;
		String operation = "";
		String rawData = "";
		int flag = -1;
		GCFactory factory = new GCFactory(heapSize, p, t);
		GC gc = factory.initializeGC(gcChoice);
		
		try {
			//System.out.println(gcChoice + " GC");
			//System.out.println("Total(B) Usage(B) Total(A) Usage(A)");
			sc = new Scanner(file);
			while (sc.hasNext()) {
				rawData = sc.nextLine();
				operation = rawData.charAt(0) + "";
				int[] extractedDataArr = parseData(rawData, operation);
				if (operation.equals(Constances.ALLOCATION)) {
					int objId = extractedDataArr[1];
					int objSize = extractedDataArr[2];
					int numOfRefSlot = extractedDataArr[3];
					Object obj = new Object(objId, objSize, numOfRefSlot);
					flag = gc.allocate(obj);
					if (flag == -1) {
						//System.out.println("Fail to allocate Object: " + obj.toString());
						int gcCounts = gc.gcCounts+1;
						System.out.println();
						System.out.print(gcCounts + ", ");
						gc.collectData(); // Before GC collect memory usage data
						gc.runGC();
						gc.collectData(); // After GC collect memory usage data
						if (gc.isExpanded()) {
							gc.increaseHeapSize();		
						}						
						
						while (gc.allocate(obj) == -1) {
							//System.out.println("Fail to allocate Object: " + obj.toString());
							gc.increaseHeapSize();
						}
						
					} else if (flag == -2) {
						// Do not allocate NULL object
						continue;
					} else {
						// object is allocated successfully
						//System.out.println("Allocate Object: " + obj.getObjId());
						continue;
					}
				} else if (operation.equals(Constances.PLUS)) {
					int objId = extractedDataArr[1];
					flag = gc.addRefToRootSet(objId);
					if (flag == -1) {
						System.out.println("Fail to add an object reference!!!");
						break;
					}

				} else if (operation.equals(Constances.DELETE)) {
					int objId = extractedDataArr[1];
					flag = gc.deleteRefFromRootSet(objId);
					if (flag == -1) {
						System.out.println("Fail to delete an object reference!!!");
						break;
					}
				} else if (operation.equals(Constances.WRITE)) {
					int parentId = extractedDataArr[1];
					int childId = extractedDataArr[3];
					int slotNum = extractedDataArr[2];
					gc.writeObjRef(parentId, childId, slotNum);
				}
			}
			System.out.println();
			gc.stats();			
		} catch (

		FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}/* catch (NumberFormatException e) {
			System.out.println("Invalid Heap Size!!!");
		}*/ finally {
			sc.close();
		}
	}
	
	private int[] parseData(String rawData, String op) {
		int[] dataArr = null;
		int dataArrIndex = 0;
		String[] rawDataArr = rawData.split(Constances.DELIMETER);

		if (op.equals(Constances.ALLOCATION)) {
			dataArr = new int[5];
		} else if (op.equals(Constances.PLUS) || op.equals(Constances.DELETE)) {
			dataArr = new int[2];
		} else if (op.equals(Constances.WRITE)) {
			dataArr = new int[7];
		}
		if (dataArr != null) {		
			for (int i = 1; i < rawDataArr.length; i++) {
				String rawDataStr = rawDataArr[i];
				dataArr[dataArrIndex] = Integer.parseInt(rawDataStr.substring(1));
				dataArrIndex++;
			}
			return dataArr;
		}
		return dataArr;
	}
}
