package assignment_project;

import optimizedRCGC.OptRCGC;

public class GCFactory {
	protected int sizeOfHeap;
	protected double p;
	protected double t;
	
	public GCFactory(int sizeOfHeap, double p, double t){
		this.sizeOfHeap = sizeOfHeap;
		this.p = p;
		this.t= t;
	}
	
	public GC initializeGC(String gcChoice){
		if(gcChoice == null){
			return null;
		}else if(gcChoice.equals(Constances.MS)){
			return new MarkSweepGC(sizeOfHeap, p, t);
		}else if(gcChoice.equals(Constances.COPY)){
			return new CopyGC(sizeOfHeap, p, t);
		}else if(gcChoice.equals(Constances.RC)){
			return new RCGC(sizeOfHeap, p, t);
		}else if(gcChoice.equals(Constances.ORC)){
			return new OptRCGC(sizeOfHeap, p, t);
		}
		return null;
	}
}
