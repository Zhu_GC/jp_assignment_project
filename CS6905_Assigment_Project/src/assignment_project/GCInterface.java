package assignment_project;

public interface GCInterface {
	public int addRefToRootSet(int objId);
	public int deleteRefFromRootSet(int objId);
	public int writeObjRef(int pId, int chId, int slotNum);
	public int allocate(Object obj);
	public void runGC();
	public void increaseHeapSize();
	public boolean isExpanded();
	public void stats();
	public void collectData();
}
