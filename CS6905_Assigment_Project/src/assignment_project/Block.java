package assignment_project;

public class Block {
	private int startPt;
	private int blkSize;
	
	public Block(int startPt, int blkSize){
		this.startPt = startPt;
		this.blkSize = blkSize;
	}

	public int getStartPt() {
		return startPt;
	}

	public void setStartPt(int startPt) {
		this.startPt = startPt;
	}

	public int getBlkSize() {
		return blkSize;
	}

	public void setBlkSize(int blkSize) {
		this.blkSize = blkSize;
	}

	public String toString(){
		int endPt = startPt + blkSize;
		String printStr = "Start Pointer: " + startPt + "\tBlock Size: " + blkSize +
							"\tEnd Pointrt: " + endPt;
		return printStr;
		 
	}

}
