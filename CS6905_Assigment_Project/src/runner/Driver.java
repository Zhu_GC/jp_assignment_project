package runner;

import assignment_project.Constances;
import assignment_project.DataProcess;

public class Driver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		if(args.length != 5){
			System.out.println("Usage: java Driver heapSize p t traceFileName gcChoice");
			System.exit(-1);
		}else{
			int heapSize = Integer.parseInt(args[0]);	
			double p = Double.parseDouble(args[1]);
			double t = Double.parseDouble(args[2]);
			String traceFileName = args[3];
			String gcChoice = args[4];
			if(gcChoice.equals(Constances.COPY) || gcChoice.equals(Constances.MS) || 
					gcChoice.equals(Constances.RC)){
				DataProcess dataPro = new DataProcess();
				dataPro.scanFile(heapSize, p, t, traceFileName, gcChoice);
			}else{
				System.out.println("Invalid GC Algorithm!!! Please Select \"copy\" or \"ms\"");
			}
			
		}
	}
	
	
}
